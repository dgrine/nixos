{ pkgs, ... }:

{
    users.users.djamelg = {
        isNormalUser = true;
        createHome = true;
        extraGroups = [
            "audio"
            "networkmanager"
            "video"
            "wheel"
        ];
        shell = pkgs.zsh;
    };

    home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        users.djamelg = {
            imports = [
                ../programs/alacritty/alacritty.nix
                ../programs/fzf/fzf.nix
                ../programs/tmux/tmux.nix
                ../programs/vifm/vifm.nix
                ../programs/zsh/zsh.nix
            ];
            home = {
                username = "djamelg";
                homeDirectory = "/home/djamelg";
                stateVersion = "23.05";
            };
            fonts.fontconfig.enable = true;
        };
    };

    # This is required at global level
    programs.zsh.enable = true;

    fonts.fonts = with pkgs; [
        (nerdfonts.override { fonts = [ "UbuntuMono" ]; })
    ];
}
