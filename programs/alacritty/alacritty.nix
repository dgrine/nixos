{
    programs.alacritty.enable = true;
    xdg.configFile."alacritty".source = ../../dotfiles/alacritty/.config/alacritty;
}
