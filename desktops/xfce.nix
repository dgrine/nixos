{ pkgs, ... }:

{
    security.pam.services.gdm.enableGnomeKeyring = true;

    services = {
        blueman.enable = true;
        gnome.gnome-keyring.enable = true;
        pipewire = {
            enable = true;
            alsa = {
                enable = true;
                support32Bit = true;
            };
            pulse.enable = true;
        };

        xserver = {
            enable = true;
            autorun = true;
            layout = "us";
            excludePackages = with pkgs; [ xterm ];
            displayManager.gdm.enable = true;
            desktopManager.xfce.enable = true;
        };
    };

    sound = {
        enable = true;
        mediaKeys.enable = true;
    };

    hardware = {
        pulseaudio.enable = false;
        bluetooth.enable = true;
    };

    
    programs = {
        dconf.enable = true;

        gnupg.agent = {
            enable = true;
            enableSSHSupport = true;
        };

        thunar = {
            enable = true;
            plugins = with pkgs.xfce; [
                thunar-archive-plugin
                thunar-media-tags-plugin
                thunar-volman
            ];
        };
    };

    environment = {
        systemPackages = with pkgs; [
            blueman
            brave
            elementary-xfce-icon-theme
            font-manager
            xfce.xfce4-panel
            xorg.xev
            xsel
        ];
    };

}
