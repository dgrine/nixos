{
    imports = [
        ./hardware-configuration.nix
        ../packages.nix
        ../../users/djamelg.nix
        ../../desktops/xfce.nix
        ../../desktops/gnome-pop-shell.nix
        # ../../desktops/gnome-xmonad.nix
        # ../../desktops/hyprland.nix
        # ../../desktops/qtile.nix
    ];

    # Boot loader
    boot.loader.grub = {
        enable = true;
        device = "/dev/vda";
        useOSProber = true;
    };

    # Networking
    networking = {
        hostName = "virt-manager";
        networkmanager.enable = true;
    };
}
