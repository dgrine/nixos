{ config, lib, pkgs, modulesPath, ... }:

{
    imports = [
        (modulesPath + "/profiles/qemu-guest.nix")
    ];
    
    boot = {
        initrd = {
            availableKernelModules = [
                "ahci"
                "xhci_pci"
                "virtio_pci"
                "sr_mod"
                "virtio_blk"
            ];
            kernelModules = [];
        };
        kernelModules = [ "kvm-amd" ];
        extraModulePackages = [];
    };

    fileSystems."/" = {
        device = "/dev/disk/by-uuid/7ead6d2b-75b5-4635-a1ff-5bcf5e45bbd2";
        fsType = "ext4";
    };

    swapDevices = [];

    nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
}
