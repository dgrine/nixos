{ pkgs, ... }:

{
    nixpkgs.config.allowUnfree = true;
    environment.systemPackages = with pkgs; [
        alacritty
        bat
        git
        neovim
        vifm
    ];
}
