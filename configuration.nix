{ ... }:

{
    imports = [
        <home-manager/nixos>
        ./host.nix
    ];

    # Always leave this value as is
    system.stateVersion = "23.05";
}
